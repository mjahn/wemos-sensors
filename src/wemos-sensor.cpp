#include <Arduino.h>
#include <FS.h>
#include <SPI.h>
#include <BME280I2C.h>
#include <Wire.h>
#include <ESP8266HTTPClient.h> //https://github.com/esp8266/Arduino
#include <WiFiManager.h>
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

BME280I2C bme;
HTTPClient http;

bool shouldSaveConfig = false;

char server_host[128];
char server_fingerprint[32];
char server_path[128];
char server_apikey[32];

float voltage(NAN);
float temperature(NAN);
float humidity(NAN);
float pressure(NAN);
int power(NAN);

BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
BME280::PresUnit presUnit(BME280::PresUnit_Pa);

WiFiManagerParameter custom_server_host("server_host", "IOT Server SSL Host", server_host, 128);
WiFiManagerParameter custom_server_fingerprint("server_fingerprint", "IOT Server SSL Fingerprint", server_fingerprint, 32);
WiFiManagerParameter custom_server_path("server_path", "IOT URL Pfad", server_path, 128);
WiFiManagerParameter custom_server_apikey("server_apikey", "IOT API-Key", server_apikey, 32);

void measureBME280() {
    bme.read(pressure, temperature, humidity, tempUnit, presUnit);

    Serial.print("Temperatur = ");
    Serial.print(temperature);
    Serial.println(" °C, ");
    Serial.print("Luftfeuchtigkeit = ");
    Serial.print(humidity);
    Serial.println(" %");
    Serial.print("Luftdruck = ");
    Serial.print(pressure);
    Serial.println(" Pa");
}

void measureVoltage() {
    int value = 0;
    value = analogRead(0);
    voltage = float(value);
    voltage = value * 3.3F / 1023;

    Serial.print("Voltage = ");
    Serial.print(voltage);
    Serial.println(" V");
}

void measurePower() {
    power = digitalRead(D3);

    Serial.print("Power = ");
    Serial.print(power);
    Serial.println(" V");
}

/**
* publish data to service for further analysis
*/
void publishData() 
{
    String postData = "";
    postData += "api_key=ELACKGLEWU6WRK9Q";
// postData += server_host;
    postData += "&field1=";
    postData += String(temperature);
    postData += "&field2=";
    postData += String(humidity);
    postData += "&field3=";
    postData += String(pressure);
    postData += "&field4=";
    postData += String(voltage);
    postData += "&field5=";
    postData += String(power);

    http.begin("https://api.thingspeak.com/update.json", "f9c2656cf9ef7f668bf735fe15ea829f5f55543e");
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");
    int code = http.POST(postData);
    http.end();

    Serial.println("POST data");
    Serial.print(postData);
    Serial.println(" ");
    Serial.print("HTTP status ");
    Serial.print(code);
    Serial.println(" ");
}

void measure() {
    measureBME280();
    measureVoltage();
    measurePower();
}

// save the custom configuration to EEPROM
void saveConfigCallback()
{
    Serial.println("Should save config");
    shouldSaveConfig = true;
}
// save the custom configuration to EEPROM
void saveConfig()
{
    Serial.println("saving config");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["server_host"] = server_host;
    json["server_fingerprint"] = server_fingerprint;
    json["server_path"] = server_path;
    json["server_apikey"] = server_apikey;

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
        Serial.println("failed to open config file for writing");
    }

    json.printTo(Serial);
    json.printTo(configFile);
    configFile.close();
}

// read the custom configuration from EEPROM
void readConfig() 
{
    if (SPIFFS.begin()) {
        Serial.println("mounted file system");
        if (SPIFFS.exists("/config.json")) {
        //file exists, reading and loading
            Serial.println("reading config file");
            File configFile = SPIFFS.open("/config.json", "r");
            if (configFile) {
                Serial.println("opened config file");
                size_t size = configFile.size();
                // Allocate a buffer to store contents of the file.
                std::unique_ptr<char[]> buf(new char[size]);

                configFile.readBytes(buf.get(), size);
                DynamicJsonBuffer jsonBuffer;
                JsonObject& json = jsonBuffer.parseObject(buf.get());
                json.printTo(Serial);
                if (json.success()) {
                    Serial.println("\nparsed json");

                    strcpy(server_host, json["server_host"]);
                    strcpy(server_fingerprint, json["server_fingerprint"]);
                    strcpy(server_path, json["server_path"]);
                    strcpy(server_apikey, json["server_apikey"]);

                } else {
                    Serial.println("failed to load json config");
                }
                configFile.close();
            }
        }
    } else {
        Serial.println("failed to mount FS");
    }
}

void initSensorBME280() {
    Wire.begin();
    while(!bme.begin())
    {
        Serial.println("Could not find BME280I2C sensor!");
        delay(1000);
    }

    switch(bme.chipModel())
    {
        case BME280::ChipModel_BME280:
        Serial.println("Found BME280 sensor! Success.");
        break;
        case BME280::ChipModel_BMP280:
        Serial.println("Found BMP280 sensor! No Humidity available.");
        break;
        default:
        Serial.println("Found UNKNOWN sensor! Error!");
    }
}

void setup() {
    Serial.begin(9600); // start serial console for debugging purpose

    //read configuration from FS json
    Serial.println("mounting FS...");

    initSensorBME280();
    readConfig();

    WiFiManager wifiManager;
    wifiManager.addParameter(&custom_server_host);
    wifiManager.addParameter(&custom_server_fingerprint);
    wifiManager.addParameter(&custom_server_path);
    wifiManager.addParameter(&custom_server_apikey);
    wifiManager.setSaveConfigCallback(saveConfigCallback);

    wifiManager.autoConnect("ion2s Sensor", "!0n2s");
}

void loop() {
    measure();
    publishData();

    ESP.deepSleep(10 * 60 * 1000000); // deepSleep time is defined in microseconds. Multiply seconds by 1e6 
    delay(200);
}